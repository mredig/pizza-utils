#!/usr/bin/env sh

echo "alias dc='podman-compose'
alias ga='git add'
alias gc='git commit -m'
alias gp='git push'
alias gs='git status'
" >> ~/.profile
