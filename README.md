# Pizza Utils

`git clone` this repo and run `install.sh` as a makeshift, easy to use and manage package manager for useful scripts I've made.

To install, run
* noninteractive
	* `/bin/bash -c "$(curl -fsSL 'https://gitlab.com/mredig/pizza-utils/-/raw/main/remote-install.sh?ref_type=heads')" - -f` 
* interactive
	* `/bin/bash -c "$(curl -fsSL 'https://gitlab.com/mredig/pizza-utils/-/raw/main/remote-install.sh?ref_type=heads')"` 
(assumes you have a compatible `sh` interpretter and common distro)